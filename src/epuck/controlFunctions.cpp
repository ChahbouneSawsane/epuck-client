#include <controlFunctions.h>

/**** Derive robot pose from encoders and save it along with robot linear and angular velocities ****/
RobPose GetCurrPoseFromEncoders(const RobPose & prevRobPose, const int& EncL, const int& EncR, const int& prevEncL, const int& prevEncR, const string & data_folder){
    printf("Left Encoder : %i ", EncL);
    printf("Right Encoder : %i \n", EncR);
    const float encoderFactor = 0.0072;//with bias
    const float sampleTime = 0.1;//TODO this should also be read from logs

    float linVel, angVel, dx, dy, dTh;
    float dEncL = EncL - prevEncL;
    float dEncR = EncR - prevEncR;
    if ((fabs(dEncL) > 150) || (fabs(dEncR) > 150)) {//fix measuring errors
        linVel = 0;
        angVel = 0;
    } else {
        linVel = rp.wheel_radius * encoderFactor * (dEncR + dEncL) / 2;
        angVel = rp.wheel_radius * encoderFactor * (dEncR - dEncL) / rp.axisLength;
    }
    dTh = angVel * sampleTime;
    dx = linVel * cos(prevRobPose.getTh() +dTh);
    dy = linVel * sin(prevRobPose.getTh() +dTh);
    RobPose curRobPose(prevRobPose.getX()+dx, prevRobPose.getY()+dy, prevRobPose.getTh() +dTh);

    //log the data
    FILE *v, *w, *Dx, *Dy, *Dth, *x, *y, *th, *time;
    v = fopen((data_folder + "v.txt").c_str(), "a+");
    w = fopen((data_folder + "w.txt").c_str(), "a+");
    Dx = fopen((data_folder + "dx.txt").c_str(), "a+");
    Dy = fopen((data_folder + "dy.txt").c_str(), "a+");
    x = fopen((data_folder + "xEnc.txt").c_str(), "a+");
    y = fopen((data_folder + "yEnc.txt").c_str(), "a+");
    th= fopen((data_folder + "thetaEnc.txt").c_str(), "a+");
    time = fopen((data_folder + "time.txt").c_str(), "a+");
    fprintf(v, "%f\n", linVel);
    fprintf(w, "%f\n", angVel);
    fprintf(Dx, "%f\n", dx/sampleTime);
    fprintf(Dy, "%f\n", dy/sampleTime);
    fprintf(x, "%f\n", curRobPose.getX());
    fprintf(y, "%f\n", curRobPose.getY());
    fprintf(th, "%f\n", curRobPose.getTh());
    fprintf(time, "%f\n", sampleTime);
    fclose(Dx);
    fclose(Dy);
    fclose(v);
    fclose(w);
    fclose(x);
    fclose(y);
    fclose(th);
    fclose(time);
    cout << "curRobPose from encoders is x " << curRobPose.getX() << " y " << curRobPose.getY() << endl;
    return(curRobPose);
}
/**** Derive robot pose from vision (from barycenter of target) and save it ****/
RobPose GetCurrPoseFromVision(const Point & baryctr, const float & theta, const float & areaPix, const string & data_folder){
    RobPose curRobPose;
    curRobPose.setPose(.32, 0., M_PI);//TODOM2

    //log the data
    FILE *x, *y, *th;
    x = fopen((data_folder + "xVis.txt").c_str(), "a+");
    y = fopen((data_folder + "yVis.txt").c_str(), "a+");
    th= fopen((data_folder + "thetaVis.txt").c_str(), "a+");
    fprintf(x, "%f\n", curRobPose.getX());
    fprintf(y, "%f\n", curRobPose.getY());
    fprintf(th, "%f\n", curRobPose.getTh());
    fclose(x);
    fclose(y);
    fclose(th);
    cout << "curRobPose from vision is x " << curRobPose.getX() << " y " << curRobPose.getY() << endl;
    return(curRobPose);
}
/**** Show image from the robot camera and save it on the disk ****/
Mat LoadAndShowImageFromDisk(const string folderName, const int& cnt_it) {
    //load the image from the disk
    char imgNameIteration[30];
    sprintf(imgNameIteration, "images/image%04d.jpg", cnt_it);
    Mat colorRawImg = imread(folderName + imgNameIteration);
    cout << "\nLoadImage loaded image " << folderName + imgNameIteration << endl;
    namedWindow( "loggedImg", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "loggedImg", 0,0); // Move window
    imshow( "loggedImg", colorRawImg);                // Show the image inside it
    return(colorRawImg);
}
/**** Load encoder measurements from logs on disk ****/
vector<int> LoadEncoderLogs(const string folderName) {
    vector<int> encData;
    //load the data from the disk
    FILE *enc;
    enc = fopen(folderName.c_str(), "r");
    cout << "Loading encoder from " << folderName << endl;
    int i=0;
    int tmp;
    while(fscanf(enc,"%d",&tmp)!=EOF) {
        encData.push_back(tmp);
        printf("i %d\t enc %d\n", i, encData[i]);
        i++;
    };
    int size = i;
    cout << "size " << size << endl;
    return(encData);
}
void DrawMapWithRobot(const RobPose & rPoseEnc, const RobPose & rPoseVis){
    Mat mapImg;
    static const int cmInPixels = 15;
    static const int xMaxInCm = 60;
    static const int yMaxInCm = 40;
    static const int xOriginInCm = 5;
    //draw empty map
    mapImg.create((xMaxInCm + xOriginInCm)*cmInPixels, yMaxInCm*cmInPixels, CV_8UC3);
    mapImg.setTo(Scalar(255,255,255));

    Point origin((yMaxInCm/2)*cmInPixels,xOriginInCm*cmInPixels);
    //draw grid
    //draw hor
    for (int i = 0; i <= 32; i++) {
        line(mapImg, origin + Point(-11*cmInPixels, i*cmInPixels), origin + Point(11*cmInPixels, i*cmInPixels), Scalar(200, 200, 200),1);
    }
    for (int i = 0; i <= 22; i++) {
        line(mapImg, origin + Point(-11*cmInPixels + i*cmInPixels, 0), origin + Point(-11*cmInPixels + i*cmInPixels, 32*cmInPixels), Scalar(200, 200, 200),1);
    }

    //draw scale
    line(mapImg, Point(xOriginInCm*cmInPixels,xOriginInCm*cmInPixels), Point(xOriginInCm*cmInPixels,(xOriginInCm + 1)*cmInPixels), Scalar(0, 100, 0),2);
    string cmText = "1 cm";
    putText(mapImg, cmText, Point(xOriginInCm*cmInPixels+3,(xOriginInCm+0.5)*cmInPixels), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 100, 0),2);

    //draw x axis
    arrowedLine(mapImg, origin, origin + Point(0, xMaxInCm/3*cmInPixels), Scalar(255, 0, 0),2,8,0,0.03);
    string xText = "X";
    putText(mapImg, xText, origin + Point(- cmInPixels, cmInPixels), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 0, 0),2);

    //draw y axis
    arrowedLine(mapImg, origin, origin + Point(yMaxInCm/3*cmInPixels, 0), Scalar(255, 0, 0),2,8,0,0.03);
    string yText = "Y";
    putText(mapImg, yText, origin + Point(cmInPixels, -cmInPixels), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 0, 0),2);

    float robRadius = 3.5*cmInPixels;
    //draw robot pose from encoders
    float xEnc = rPoseEnc.getX();
    float yEnc = rPoseEnc.getY();
    float thEnc  = rPoseEnc.getTh();
    float uEnc = (yEnc*100 + yMaxInCm/2)*cmInPixels;
    float vEnc = (xEnc*100 + xOriginInCm)*cmInPixels;
    line(mapImg, Point(uEnc,vEnc), Point(uEnc-robRadius*sin(thEnc-M_PI),vEnc-robRadius*cos(thEnc-M_PI)), Scalar(0, 0, 255),2);
    circle(mapImg, Point(uEnc,vEnc), robRadius, Scalar(0, 0, 255), 2);

    //draw robot pose from vision
    float xVis = rPoseVis.getX();
    float yVis = rPoseVis.getY();
    float thVis  = rPoseVis.getTh();
    float uVis = (yVis*100 + yMaxInCm/2)*cmInPixels;
    float vVis = (xVis*100 + xOriginInCm)*cmInPixels;
    line(mapImg, Point(uVis,vVis), Point(uVis-robRadius*sin(thVis-M_PI),vVis-robRadius*cos(thVis-M_PI)), Scalar(0, 0, 0),1);
    circle(mapImg, Point(uVis,vVis), robRadius, Scalar(0, 0, 0), 1);

    //show map
    namedWindow( "map", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "map", 1020,0); // Move window
    imshow( "map", mapImg);                // Show the image inside it
}
Point ProcessImageToGetBarycenter(const Mat& colRawImg, float & areaPix) {
    struct timeval start, end;
    gettimeofday(&start, NULL); //get start time of function
    Mat greyImg;
    cvtColor(colRawImg, greyImg, cv::COLOR_BGR2GRAY); //convert image to black and white

    //process images to get barycenter
    Mat processedImg;
    //TODOM2 below are the default coordinates - to be changed
    cvtColor(greyImg, processedImg, cv::COLOR_GRAY2BGR);//convert image to color (for displaying colored features)
    Point targetBarycenter(160,120);
    areaPix = 1;

    //cout << "targetBarycenter is at " << targetBarycenter << endl; //print coordinates
    circle(processedImg, targetBarycenter, 3, Scalar(0, 0, 255), 1); 	//display red circle
    circle(greyImg, targetBarycenter, 3, Scalar(255, 255, 255), 1); 	//display white circle

    //show the images
    namedWindow( "greyImg", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "greyImg", 340,0); // Move window
    imshow( "greyImg", greyImg);                // Show the image inside it
    namedWindow( "processedImg", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "processedImg", 680,0); // Move window
    imshow( "processedImg", processedImg);                // Show the image inside it

    gettimeofday(&end, NULL); //get end time
    double time_spent = (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_usec -
                                                             start.tv_usec) * 1e-3;
    printf("ProcessImageToGetBarycenter took %f ms\n", time_spent); //print time spetn for processing
    return(targetBarycenter);
}
/**** Send commands (speed_L, speed_R) to motors in order to realize robot
 * velocities (v, w) for 10 seconds ****/
void SetRobotVelocities(struct timeval startTime, const float& vel, const float& omega, char MotorCmd[15]) {
    printf("The required robot velocities are v %f w %f\n\n\n", vel, omega);
    struct timeval curTime;
    gettimeofday(&curTime, NULL);
    long int timeSinceStart =
            ((curTime.tv_sec * 1000000 + curTime.tv_usec) -
             (startTime.tv_sec * 1000000 + startTime.tv_usec)) /
            1000;
    printf("timeSinceStart = %ld ms\n", timeSinceStart);
    // Commands to be sent to the left and right wheels
    int speedL;
    int speedR;
    const float encFactor  = 0.0054;
    if (timeSinceStart < 10000) {
        speedL = (2*vel - rp.axisLength * omega) / (rp.wheel_radius * encFactor);//TODO there is a 2 * error factor on pure rotation (robot pivots twice faster than required)
        speedR = (2*vel + rp.axisLength * omega) / (rp.wheel_radius * encFactor);
        // Sends the command to the epuck motors
        sprintf(MotorCmd, "D,%d,%d", speedL, speedR);
    } else {
        sprintf(MotorCmd, "D,%d,%d", 0, 0);
    }
    printf("SetRobotVelocities wheel speeds are %d %d ", speedL, speedR);
}

// control robot using images from the camera
void ControlRobotWithVisualServoing(const Point &baryc, float &vel, float &omega) {
    //TODOM2
    //calculate vel and omega given baryc
    vel = 0;
    omega = 0;
}
// make robot follow a wall using infrared measurements for ten seconds
void ControlRobotToFollowWall(struct timeval startTime, float &vel, float &omega) {
    // replace the lines below with commands v and w needed to follow wall
    // from xA, yA, xB et yB
    // Load these variables from files xA.txt etc...
    //printf("infrared measures xA %f yA %f xB %f yB %f \n", xA, yA, xB, yB);
    // calculate equation of the line in the robot frame
    float mMur, pMur; // parametres definissant le mur dans le repere robot
    printf("equation of the line in the robot frame: y = %f x + %f \n", mMur,
           pMur);
    // measure the time
    struct timeval curTime;
    gettimeofday(&curTime, NULL);
    long int timeSinceStart =
            ((curTime.tv_sec * 1000000 + curTime.tv_usec) -
             (startTime.tv_sec * 1000000 + startTime.tv_usec)) /
            1000;
    printf("timeSinceStart = %ld ms\n", timeSinceStart);
    if (timeSinceStart < 10000) {
        vel = 0;
        omega = 0;
    } else {
        vel = 0;
        omega = 0;
    }
}
void ProcessInfraRed(const int ProxS[]) {
    // infrarouges
    for (int i = 0; i < 10; i++) {
        printf("IRed %d : %i  ", i, ProxS[i]);
    }
    printf("\n");
        float dist[8];
    for (int i = 0; i < 8; i++) {
        dist[i] = 1.79 * pow(ProxS[i], -.681);
    }
    for (int i = 8; i < 10; i++) {
        dist[i] = 0.1495 * exp(-.002875 * ProxS[i]) +
                  0.05551 * exp(-.0002107 * ProxS[i]);
    }
    float x5 = (dist[5] + rp.robRadius) * cos(rp.theta[5]);
    float y5 = (dist[5] + rp.robRadius) * sin(rp.theta[5]);
    float x6 = (dist[6] + rp.robRadius) * cos(rp.theta[6]);
    float y6 = (dist[6] + rp.robRadius) * sin(rp.theta[6]);
    float x8 = (dist[8] + rp.robRadius) * cos(rp.theta[8]);
    float y8 = (dist[8] + rp.robRadius) * sin(rp.theta[8]);
    float x9 = (dist[9] + rp.robRadius) * cos(rp.theta[9]);
    float y9 = (dist[9] + rp.robRadius) * sin(rp.theta[9]);
    float aLeft, aFront, bLeft, bFront;
    //    printf("points 8 %f %f and 9 %f %f\n",x8,y8,x9,y9);
    // Valeurs des encodeurs, vitesse et capteurs de proximite
    float xRobot, yRobot, thetaRob;

    if (fabs(x8 - x9) > 0.00001) {
        aFront = (y8 - y9) / (x8 - x9);
        bFront = (x8 * y9 - x9 * y8) / (x8 - x9);
        xRobot = fabs(bFront) / sqrt(1 + aFront * aFront) + 0.025; // TODO FIX
    } else {
        xRobot = x8 + 0.025; // TODO FIX
                             // aFront = (y8-y9)/(sign(x8-x9)*.00001);
                             // bFront = (x8*y9-x9*y8)/(sign(x8-x9)*.00001);
    }
    //    printf("front line a %f b %f \n", aFront, bFront);
    if (fabs(x5 - x6) > 0.00001) {
        aLeft = (y5 - y6) / (x5 - x6);
        bLeft = (x5 * y6 - x6 * y5) / (x5 - x6);
    } else {
        if ((x5 - x6) > 0) {
            aLeft = (y5 - y6) / .00001;
            bLeft = (x5 * y6 - x6 * y5) / .00001;
        } else {
            aLeft = -(y5 - y6) / .00001;
            bLeft = -(x5 * y6 - x6 * y5) / .00001;
        }
    }
    yRobot = fabs(bLeft) / sqrt(1 + aLeft * aLeft) + 0.045; // TODO FIX
    //    printf("left line a %f b %f \n",aFront,bFront);
    //    printf("xRob %f yRob %f\n",xRobot,yRobot);
}
/**** Projection mesures infrarouges dans repere robot - fonction specifique de
 * ce programme****/
void ProjectionInfrarougesDansRepereRobotEtSauvetage(const string & data_folder, const int ProxS[]) {
    // infrarouges
    float dist[8];
    for (int i = 0; i < 8; i++) {
        dist[i] = 1.79 * pow(ProxS[i], -.681);
    }
    for (int i = 8; i < 10; i++) {
        dist[i] = 0.1495 * exp(-.002875 * ProxS[i]) + 0.05551 * exp(-.0002107 * ProxS[i]);
    }
    dist[6] = dist[6] + 0.01;
    float xA, yA, xB, yB;
    xA = (dist[5] + rp.robRadius) * cos(rp.theta[5]);
    yA = (dist[5] + rp.robRadius) * sin(rp.theta[5]);
    xB = (dist[6] + rp.robRadius) * cos(rp.theta[6]);
    yB = (dist[6] + rp.robRadius) * sin(rp.theta[6]);

    FILE *x_A, *y_A, *x_B, *y_B;
    x_A = fopen((data_folder + "xA.txt").c_str(), "a+");
    y_A = fopen((data_folder + "yA.txt").c_str(), "a+");
    x_B = fopen((data_folder + "xB.txt").c_str(), "a+");
    y_B = fopen((data_folder + "yB.txt").c_str(), "a+");
    fprintf(x_A, "%f\n", xA);
    fprintf(y_A, "%f\n", yA);
    fprintf(x_B, "%f\n", xB);
    fprintf(y_B, "%f\n", yB);
    fclose(x_A);
    fclose(y_A);
    fclose(x_B);
    fclose(y_B);
}
