#pragma once

/* CLIENT FOR EPUCK ROBOT*/

#include <arpa/inet.h>
#include <cstdio> // Pour les Sprintf
#include <fstream>
#include <iostream>
#include <netinet/in.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctime>
#include <unistd.h>
#include <signal.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

using namespace std;
using namespace cv;

/* Variables of main thread */

typedef int SOCKET;
SOCKET CameraSocket, CommandSendingSocket, SensorReceivingSocket;
char AllSensors[100]; // Buffer de reception des capteurs
int EncoderL, EncoderR, prevEncoderL, prevEncodeurR;
int ProxSensors[10];
bool stop_threads = false;
std::string images_log_folder;

// Camera variables
bool CameraActive = false;
struct imageData {
    int id_img;
    int id_block;
    unsigned long date;
    unsigned char msg[230400];
} imgData;

// General functions
void sigint_handler(int signal);

inline int sign(float val);

// Functions for opening sockets
void InitSocketOpening(const std::string& epuck_ip);

void OpenCameraSocket();

void OpenSensorReceivingSocket();

/* Socket EnvoieCommandes */
void OpenCommandSendingSocket(const std::string& epuck_ip);

/**** Fonction de fermeture de socket ****/
void CloseSocket(int NOM_SOCKET);

void SendMotorAndLEDCommandToRobot(const char MotorCmd[15]);

void ReceiveSensorMeasures();

void SplitSensorMeasures();

/**** fonction initialisation Camera ****/
void InitCamera(const std::string& epuck_ip);

/*****************/
/**** Threads ****/
/*****************/

/**** Thread for receiving camera data ****/
void* CameraReceptionThread(void* arg);

// SPECIFIC FUNCTIONS
void SaveData(const string & data_folder);

/**** Send commands to the wheel motors for 10 seconds****/
void SetWheelCommands(struct timeval startTime, const int& speed_L,
                      const int& speed_R, char MotorCmd[15]);

/**** Function that creates a log folder ****/
std::string createLogFolder();
