# E-Puck client

This project offer a UDP client to communicate with an E-Puck robot. It allows to send motor commands and receive the data comming from all the sensors, including the camera.

# How to use

Fork and clone this project, fill the blanks in the application and try to run it: 
```
cd build
cmake ..
cmake --build .    # add --parallel for parallel compilation
```

The application can be found in in `build/apps/epuck-app`.
