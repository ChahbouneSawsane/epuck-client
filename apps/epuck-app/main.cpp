#include <epuck.h>
#include <controlFunctions.h>


/**** Show image from the robot camera and save it on the disk ****/
Mat ShowAndSaveRobotImage(unsigned char* imgData, const int& cnt_it) {
    Mat rawImg =  Mat(240, 320, CV_8UC3, imgData);
    Mat img;
    cvtColor(rawImg, img, COLOR_RGB2BGRA); 
    //show the image
    namedWindow( "Robot image", WINDOW_AUTOSIZE ); // Create a window for display.
    moveWindow( "Robot image", 0,0); // Move window 
    imshow( "Robot image", img );                // Show the image inside it.
    waitKey(1);
    //save the image on disk
    char imgNameIteration[20];
    sprintf(imgNameIteration, "image%04d.jpg", cnt_it);
    imwrite(images_log_folder + imgNameIteration, img);
    return(img);
}


/*****************************/
/**** Main Program ***********/
/*****************************/
pthread_t IDCameraReceptionThread, IDMainThread;

std::string data_log_folder;

void IncorrectArguments(const int& argc);
void* MainThread(void* param);

int main(int argc, char** argv) {
    if (argc < 3)
        IncorrectArguments(argc);
    
    data_log_folder = createLogFolder();
    
    CameraActive = true; // camera active

    std::string epuck_ip = argv[2];
    InitCamera(epuck_ip); // sends camera state info to epuck
    // Open all Sockets
    if (CameraActive == true) {
        OpenCameraSocket();
    }
    OpenSensorReceivingSocket();
    OpenCommandSendingSocket(epuck_ip);

    signal(SIGINT, sigint_handler);
    // Creation of the camera reception thread
    if (CameraActive == true) {
        if (pthread_create(&IDCameraReceptionThread, NULL,
                           CameraReceptionThread, NULL) == -1) {
            printf("Error while creating the camera reception thread!\n");
        } else {
            printf("Camera reception thread succesfully created!\n");
        }
    }
    // Creation of the main thread
    if (pthread_create(&IDMainThread, NULL, MainThread, argv) == -1) {
        printf("Error while creating the main thread!\n");
    } else {
        printf("Main thread succesfully created!\n");
    }
    // Terminate both threads
    if (CameraActive == true) {
        pthread_join(IDCameraReceptionThread, NULL);
    }
    pthread_join(IDMainThread, NULL);
    signal(SIGINT, nullptr);
    // Close all sockets
    if (CameraActive == true) {
        CloseSocket(CameraSocket);
    }
    CloseSocket(CommandSendingSocket);
    CloseSocket(SensorReceivingSocket);
    return 0;
}


void* MainThread(void* param) {
    printf("\nINIT \n");
    char** argv = (char**)param;
    int argc = 0;
    while (argv[argc] != NULL) {
        argc++;
    }
    enum Controller { setWheelCmd, setRobVel, followWall, visServo };
    Controller c;
    std::string cont = argv[1];
    std::string epuck_ip = argv[2];
    std::string folderNameForImages;
    int speed_L, speed_R;
    float vel, omega;

    if (cont == "setWheelCmd") {
        if (argc != 5) {
            IncorrectArguments(argc);
        } else {
            c = setWheelCmd;
            speed_L = strtol(argv[3], NULL, 10);
            speed_R = strtol(argv[4], NULL, 10);
        }
    } else if (cont == "setRobVel") {
        if (argc != 5) {
            IncorrectArguments(argc);
        } else {
            c = setRobVel;
            vel = strtod(argv[3], NULL);
            omega = strtod(argv[4], NULL);
        }
    } else if (cont == "followWall") {
        if (argc != 3)
            IncorrectArguments(argc);
        else
            c = followWall;
    } else if (cont == "visServo") {
        if (argc != 3)
            IncorrectArguments(argc);
        else
            c = visServo;
    } else {
        IncorrectArguments(argc);
    }
    printf("Controller is %s\n", argv[1]);
    int cnt_iter = 1; // to get the current iteration
    struct timeval startTime, curTime, prevTime;
    double timeSinceStart;
    gettimeofday(&startTime, NULL); // get starting time
    char MotorCommand[15];      // command for the two motors
    Mat robImg;
    RobPose prevPoseFromEnc, curPoseFromEnc, prevPoseFromVis, curPoseFromVis, initPose;
    initPose.setPose(.32, 0., M_PI);

    while (not stop_threads) {
        printf("\033[1;36m");//write in bold cyan
        printf("\nSTART ITERATION %d \n", cnt_iter);
	printf("\033[0m");//reset color
	gettimeofday(&prevTime, NULL);
    	//show and save image
        if (CameraActive == true) {
            robImg = ShowAndSaveRobotImage(imgData.msg, cnt_iter);
        }
    	gettimeofday(&curTime, NULL);
    	timeSinceStart = (curTime.tv_sec - prevTime.tv_sec) * 1e3 + (curTime.tv_usec - prevTime.tv_usec) * 1e-3;
    	//timeSinceStart = ((curTime.tv_sec * 1000000 + curTime.tv_usec) - (prevTime.tv_sec * 1000000 + prevTime.tv_usec)) / 1000;
    	printf("IP\ttimeSinceStart = %f ms\n", timeSinceStart);
	gettimeofday(&prevTime, NULL);


        if(cnt_iter == 1) {
            curPoseFromEnc = initPose;
            curPoseFromVis = initPose;
        } else {
            curPoseFromEnc = GetCurrPoseFromEncoders(prevPoseFromEnc, EncoderL, EncoderR, prevEncoderL, prevEncodeurR, data_log_folder);
        }
        float areaPix;
        Point baryc = ProcessImageToGetBarycenter(robImg, areaPix);
        curPoseFromVis = GetCurrPoseFromVision(baryc, curPoseFromEnc.getTh(), areaPix, data_log_folder);
        prevPoseFromEnc = curPoseFromEnc;
        prevPoseFromVis = curPoseFromVis;
        DrawMapWithRobot(curPoseFromEnc, curPoseFromVis);

        //control robot
        if (c == setWheelCmd) {
            SetWheelCommands(startTime, speed_L, speed_R, MotorCommand); // send commmands to wheels
        } else if (c == setRobVel) {
            SetRobotVelocities(startTime, vel, omega, MotorCommand); // send operational velocities to robot
        } else if (c == followWall) {
            ProjectionInfrarougesDansRepereRobotEtSauvetage(data_log_folder, ProxSensors);
            ControlRobotToFollowWall(startTime, vel, omega); // make robot follow a wall using infrared measurements
            SetRobotVelocities(startTime, vel, omega, MotorCommand); // send operational velocities to robot
        } else if (c == visServo) {
            ControlRobotWithVisualServoing(baryc, vel, omega); // control robot using images from the camera
            SetRobotVelocities(startTime, vel, omega, MotorCommand); // send operational velocities to robot
        }

        ReceiveSensorMeasures(); // receive data from encoders and proximity sensors //TODO move these four lines above so they work above
        SplitSensorMeasures(); // splits measures and converts them to integer
        ProcessInfraRed(ProxSensors);
        SaveData(data_log_folder);

        SendMotorAndLEDCommandToRobot(MotorCommand);

        cnt_iter++;

    	gettimeofday(&curTime, NULL);
    	timeSinceStart = (curTime.tv_sec - prevTime.tv_sec) * 1e3 + (curTime.tv_usec - prevTime.tv_usec) * 1e-3;

        printf("proc\ttimeSinceStart = %f ms\n", timeSinceStart);
	gettimeofday(&prevTime, NULL);

    }

    // Send a zero velocity command before exiting
    SetWheelCommands(startTime, 0, 0, MotorCommand);
    SendMotorAndLEDCommandToRobot(MotorCommand);
    printf("All good, mate\n");
    pthread_exit(NULL);
}

void IncorrectArguments(const int& argc) {
    printf("There are %d arguments instead of 2 or 4\n", argc - 1);
    printf(
        "The first argument should be one of the "
        "following:"
        "\n\tsetWheelCmd\n\tsetRobVel\n\tfollowWall\n\tvisServo\n");
    printf("The following arguments should be:\n");
    printf("\tsetWheelCmd: IP leftWheelCmd rightWheelCmd\n");
    printf("\tsetRobVel: IP v(linear vel) w(angular vel)\n");
    printf("\tfollowWall: IP\n");
    printf("\tvisServo: IP\n");
    exit(0);
}
