#include <epuck.h>
#include <controlFunctions.h>


/*****************************/
/**** Main Program ***********/
/*****************************/

void IncorrectArguments(const int& argc);

int main(int argc, char** argv) {
    if (argc != 2) {
        IncorrectArguments(argc);
    }
    std::string nameOfReadFolder = argv[1];
    int cnt = 1;
    RobPose prevPoseFromEnc, curPoseFromEnc, prevPoseFromVis, curPoseFromVis, initPose;
    std::vector<int> encL, encR;
    encL = LoadEncoderLogs(nameOfReadFolder + "data/eg.txt");
    encR = LoadEncoderLogs(nameOfReadFolder + "data/ed.txt");
    std::string nameOfWriteFolder = createLogFolder();
    Mat colImgFromDisk;
    initPose.setPose(.32, 0., M_PI);
    while(true){
        printf("\033[1;36m");//write in bold cyan
        printf("\nSTART ITERATION %d \n", cnt);
        printf("\033[0m");//reset color

        if(cnt == 1) {
            curPoseFromEnc = initPose;
            curPoseFromVis = initPose;
            colImgFromDisk = LoadAndShowImageFromDisk(nameOfReadFolder, 2);
        } else {
            curPoseFromEnc = GetCurrPoseFromEncoders(prevPoseFromEnc, encL[cnt-1], encR[cnt-1], encL[cnt-2], encR[cnt-2], nameOfWriteFolder);
            colImgFromDisk = LoadAndShowImageFromDisk(nameOfReadFolder, cnt);
        }
        float areaPix;
        Point baryc = ProcessImageToGetBarycenter(colImgFromDisk, areaPix);
        curPoseFromVis = GetCurrPoseFromVision(baryc, curPoseFromEnc.getTh(), areaPix, nameOfWriteFolder);
        prevPoseFromEnc = curPoseFromEnc;
        prevPoseFromVis = curPoseFromVis;
        DrawMapWithRobot(curPoseFromEnc, curPoseFromVis);
        waitKey(0);
        cnt++;
    }
    return 0;
}


void IncorrectArguments(const int& argc) {
    printf("There are %d arguments instead of 1\n", argc - 1);
    printf("The argument should be the path to the folder containing the images to be processed \n");
    exit(0);
}
